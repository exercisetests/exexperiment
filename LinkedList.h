#ifndef LINKEDKIST_H
#define LINKEDKIST_H

/* a link that contains a positive integer value */
typedef struct Link
{
	unsigned int value;
	struct Link* next;
} Link;

typedef struct LinkedList
{
	Link* head;
} LinkedList;

void initList(LinkedList* list); // initialize list
void add(LinkedList* list, unsigned int newValue); // add new link in the beginning of list with newValue in it
int removeHead(LinkedList* list); // return the value from the first link in list, and change the first link to head->next. return -1 if list is empty
void cleanList(LinkedList* list); // delete all links from memory

#endif // LINKEDKIST_H