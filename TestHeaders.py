import CppHeaderParser

header = CppHeaderParser.CppHeader("HeaderCheck\\Queue.h")

print("Functions found")

functions = header.functions

# Print the names of the functions
for function in functions:
    signature = function["rtnType"] + " " + function["name"] + "("
    parameters = []
    for param in function["parameters"]:
        param_str = param["type"] + " " + param["name"]
        parameters.append(param_str)
    signature += ", ".join(parameters) + ")"
    print(signature)


print("\n\n")

[("Queue","initQueue", "q", "Queue*"), ("initQueue", "q", "Queue&"),("cleanQueue", "q", "Queue*"),("cleanQueue", "q", "Queue&"), ("enqueue", "q", "Queue*"),("enqueue", "q", "Queue&"), ("dequeue", "q", "Queue*"),("dequeue", "q", "Queue&"), ("isEmpty", "q", "Queue*"),("isEmpty", "q", "Queue&"), ("isFull", "q", "Queue*"),("isFull", "q", "Queue&")]

Queue::initQueue::q <Queue*> || <Queue&>
::cleanQueue::q <Queue*> || <Queue&>
::enqueue::q <Queue*> || <Queue&>
Queue::dequeue::q <Queue*> || <Queue&>
::isEmpty::q <Queue*> || <Queue&>