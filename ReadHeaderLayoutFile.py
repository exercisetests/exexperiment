
import re

class CheckerInfo:

    def __init__(self, formatFile, delimeter) :
        
        # File name
        self.fileName = self.getFileNameFromPath(formatFile)

        # Files to check
        self.filesToCheck = self.getSectionItemsFromFormatFile(formatFile, "FilesToCheck", delimeter)

        # Structs/Classes to check
        self.structsOrClassesToCheck = self.getSectionItemsFromFormatFile(formatFile, "RequiredStructOrClasses", delimeter)

        # Structs/Classes variables to check
        self.classVarsToCheck = self.getSectionItemsFromFormatFile(formatFile, "RequiredStructOrClassVariables", delimeter)
        self.convertClassesVarsListToTuples()
        
        # Functions/Methods to check
        self.functionsToCheck = self.getSectionItemsFromFormatFile(formatFile, "RequiredFunctions", delimeter)
        self.convertFunctionsListToTuples()

        # Const function/method parameters that need to be defined as 'const'
        self.constParamsToValidate = self.getSectionItemsFromFormatFile(formatFile, "ConstParameters", delimeter)
        self.convertConstParamsListToTuples()

        # Function/method parameters that need to be defined as reference or pointer
        self.referenceOrPointerParamsToValidate = self.getSectionItemsFromFormatFile(formatFile, "ReferenceParameters", delimeter)
        self.convertRefParamsListToTuples()

        # Class methods that should be defined as 'const'
        self.constMethodsToValidate = self.getSectionItemsFromFormatFile(formatFile, "ConstMethods", delimeter)
        self.convertConstMethodsListToTuples()

        # Files to be included
        self.includeLinesToValidate = self.getSectionItemsFromFormatFile(formatFile, "Includes", delimeter)

    def __str__(self):
        return  "File name: " + str(self.fileName+ '\n') +  \
                ("Files to check: " +  str(self.filesToCheck)+ '\n') +  \
                ("Structs or classes: " +  str(self.structsOrClassesToCheck)+ '\n') +  \
                ("Class or struct variables to check: " +  str(self.classVarsToCheck)+ '\n') +  \
                ("Functions to check: " +  str(self.functionsToCheck)+ '\n') +  \
                ("Const parameters to validate =" +  str(self.constParamsToValidate)+ '\n') +  \
                ("Reference or pointer parameters to validate: " +  str(self.referenceOrPointerParamsToValidate)+ '\n') +  \
                ("Const methods to validate: " +  str(self.constMethodsToValidate)+ '\n') +  \
                ("Includes: " +  str(self.includeLinesToValidate) + '\n')
    
    def __repr__(self):
        return self.__str__(self)

    def removeNewLineCharFromString(self, string):
        if(str(string).endswith("\n")):
            return string[:-1]
        
    def getFileNameFromPath(self, filePath):
        fileName = str(filePath).rsplit('\\', 1)[1]
        return fileName
    
    def getSectionItemsFromFormatFile(self, format_file, sectionHeader, delimeter):

        # Reads from format file

        with open(format_file, 'r') as file:
            lines = file.readlines()

        itemsList = []

        inSection = False

        # Reads items from the requested section, line after line

        for line in lines:
            
            if not inSection and line.startswith(delimeter + sectionHeader):
                inSection = True
                continue
            
            if(line.startswith(delimeter) and inSection):
                return itemsList

            if(inSection and line != "\n"):
                itemsList.append(self.removeNewLineCharFromString(line))
        
        # In case this is the last section
        return itemsList

    def convertClassesVarsListToTuples(self):
        newClassVarsList = []

        for element in self.classVarsToCheck:
            
            # REGEX for pattern - namespace::variable_name <variable_type>
            match = re.match(r'^(.+?)::(.+?) <(.+?)>$', element)
            if match:
                newClassVarsList.append((match.group(1), match.group(2), match.group(3)))

        self.classVarsToCheck = newClassVarsList

    def convertFunctionsListToTuples(self):
        newFunctionList = []

        for element in self.functionsToCheck:

            # REGEX for pattern - namespace::function_name <return type>
            match = re.match(r'^(?:([^:]+)::)?(.+?)::(.+)$', element)
            if match:
                if(match.group(2).startswith("::")):
                    newFunctionList.append((match.group(1) or '', match.group(2)[2:], match.group(3)))
                else:
                    newFunctionList.append((match.group(1) or '', match.group(2), match.group(3)))

        self.functionsToCheck = newFunctionList

    def convertConstParamsListToTuples(self):
        newConstParamsList = []

        for element in self.constParamsToValidate:

            # REGEX for pattern - namespace::function_name <return type>
            match = re.match(r'^(?:([^:]+)::)?(.+?)::(.+)$', element)
            if match:
                if(match.group(2).startswith("::")):
                    newConstParamsList.append((match.group(1) or '', match.group(2)[2:], match.group(3)))
                else:
                    newConstParamsList.append((match.group(1) or '', match.group(2), match.group(3)))

        self.constParamsToValidate = newConstParamsList

    def convertRefParamsListToTuples(self):
        newRefParamsList = []

        for element in self.referenceOrPointerParamsToValidate:
            match = re.match(r'(.*)(::)(.*)(::)(.*)(<)(.*)(>)', element)
            if match:
                groups = match.groups()
                namespace = groups[0] if groups[0] else ''
                functionName = groups[2]
                parameterName = groups[4]
                argumentType = groups[6]
                newRefParamsList.append((namespace, functionName, parameterName, argumentType+"&"))
                newRefParamsList.append((namespace, functionName, parameterName, argumentType+"*"))

        self.referenceOrPointerParamsToValidate = newRefParamsList

    def convertConstMethodsListToTuples(self):
        newConstMethodsList = []

        for element in self.constMethodsToValidate:

            # REGEX for pattern - namespace::function_name <return type>
            match = re.match(r'(.*)(::)(.*)(::)(.*)', element)
            if match:
                if(match.group(2).startswith("::")):
                    groups = match.groups()
                    namespace = groups[0]
                    functionName = groups[2]
                    returnType = groups[4]
                    newConstMethodsList.append((namespace, functionName, returnType))

        self.constMethodsToValidate = newConstMethodsList

format_file = "exexperiment\HeaderLayoutEx1.txt"  # Update with the path to your format file

checker = CheckerInfo(format_file, "##")

print (str(checker))