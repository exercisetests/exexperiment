#!/usr/bin/env python
import pprint
import sys
import CppHeaderParser

header = CppHeaderParser.CppHeader("exexperiment\\Utils.h")

print(header.functions + "\n\n\n")

for function in header.functions:
    print(str(function['name']) + "\n")